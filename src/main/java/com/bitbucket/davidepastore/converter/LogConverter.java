package com.bitbucket.davidepastore.converter;

import java.util.ArrayList;
import java.util.List;

import com.bitbucket.davidepastore.entity.Log;

/**
 * Converter for {@link Log} and {@link com.bitbucket.davidepastore.model.Log} classes.
 * @author Davide Pastore
 *
 */
public class LogConverter {
	
	/**
	 * From model to entity.
	 * @param log The log to convert.
	 * @return Returns the converted {@link Log}.
	 */
	public static com.bitbucket.davidepastore.model.Log fromEntityToModel(Log log) {
		com.bitbucket.davidepastore.model.Log model = new  com.bitbucket.davidepastore.model.Log();
		model.setId(log.getId());
		model.setLevel(log.getLevel());
		model.setText(log.getText());
		return model;
	}
	
	/**
	 * From model {@link List} to model {@link List}.
	 * @param logs The logs to convert.
	 * @return Returns the converted {@link Log} list.
	 */
	public static List<com.bitbucket.davidepastore.model.Log> fromEntityToModel(List<Log> logs){
		List<com.bitbucket.davidepastore.model.Log> entityLogs = new ArrayList<com.bitbucket.davidepastore.model.Log>();
		for (Log log : logs) {
			entityLogs.add(fromEntityToModel(log));
		}
		
		return entityLogs;
	}

}
