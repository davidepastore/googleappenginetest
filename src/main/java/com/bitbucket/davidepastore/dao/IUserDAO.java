package com.bitbucket.davidepastore.dao;

import com.bitbucket.davidepastore.entity.User;

public interface IUserDAO {
	User getByEmail(String email);

	User getByEmailAndPassword(String email, String password);

	boolean exists(String email, String password);

	boolean exists(String email, Boolean isGoogle);

	void create(User user);
}
