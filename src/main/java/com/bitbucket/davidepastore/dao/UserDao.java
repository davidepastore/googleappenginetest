package com.bitbucket.davidepastore.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bitbucket.davidepastore.entity.User;


@Transactional
@Repository
public class UserDAO implements IUserDAO {
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public boolean exists(String email, String password) {
		String hql = "FROM User WHERE email = ? and password = SHA2(?, 512)";
		int count = entityManager
				.createQuery(hql)
				.setParameter(1, email)
				.setParameter(2, password)
				.getResultList()
				.size();
		return count > 0 ? true : false;
	}
	
	@Override
	public User getByEmail(String email) {
		String hql = "FROM User WHERE email = ?";
		return (User) entityManager
				.createQuery(hql)
				.setParameter(1, email)
				.getSingleResult();
	}

	@Override
	public User getByEmailAndPassword(String email, String password) {
		String hql = "FROM User WHERE email = ? and password = SHA2(?, 512)";
		return (User) entityManager
				.createQuery(hql)
				.setParameter(1, email)
				.setParameter(2, password)
				.getSingleResult();
	}
	
	@Override
	public boolean exists(String email, Boolean isGoogle) {
		String hql = "FROM User WHERE email = ? and isGoogle = ?";
		int count = entityManager
				.createQuery(hql)
				.setParameter(1, email)
				.setParameter(2, isGoogle)
				.getResultList()
				.size();
		return count > 0 ? true : false;
	}

	@Override
	public void create(User user) {
		entityManager.persist(user);
	}
}
