package com.bitbucket.davidepastore.dao;

import java.util.List;

import com.bitbucket.davidepastore.entity.Log;

public interface ILogDAO {

	void create(Log log);
	
	List<Log> getAll();
}
