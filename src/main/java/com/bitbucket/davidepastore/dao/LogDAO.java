package com.bitbucket.davidepastore.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bitbucket.davidepastore.entity.Log;


@Transactional
@Repository
public class LogDAO implements ILogDAO {
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void create(Log log) {
		entityManager.persist(log);
	}

	@Override
	public List<Log> getAll() {
		String hql = "FROM Log";
		return entityManager
				.createQuery(hql)
				.getResultList();
	}
}
