package com.bitbucket.davidepastore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bitbucket.davidepastore.constant.Constant;
import com.bitbucket.davidepastore.constant.LogLevel;
import com.bitbucket.davidepastore.constant.Message;
import com.bitbucket.davidepastore.entity.User;
import com.bitbucket.davidepastore.model.Response;
import com.bitbucket.davidepastore.service.ILogService;
import com.bitbucket.davidepastore.service.IUserService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Controller
@RequestMapping("api")
public class UserController {
	@Autowired
	private IUserService userService;
	
	@Autowired
	private ILogService logService;

	@PostMapping("login")
	public ResponseEntity<Response> login(@RequestBody User user) {
		boolean exists = userService.exists(user.getEmail(), user.getPassword());
		Response response = new Response();
		if (exists) {
			logService.create("User with email " + user.getEmail() + " found on login", LogLevel.INFO);
			user = userService.getByEmailAndPassword(user.getEmail(), user.getPassword());
			response.setMessage(Message.USER_FOUND.getMessage());
			
			String compactJws = Jwts.builder()
			  .setSubject(user.getEmail())
			  .signWith(SignatureAlgorithm.HS512, Constant.SECRET_KEY)
			  .compact();
			
			MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
			headers.add("Authorization", "Bearer " + compactJws);
			return new ResponseEntity<Response>(response, headers , HttpStatus.OK);
		}
		
		logService.create("User with email " + user.getEmail() + " not found on login", LogLevel.WARNING);
		response.setMessage(Message.USER_NOT_FOUND.getMessage());
		return new ResponseEntity<Response>(response, HttpStatus.UNAUTHORIZED);
	}
	
	@PostMapping("google-login")
	public ResponseEntity<Response> googleLogin(@RequestBody User user) {
		// Check if the user exists
		boolean exists = userService.existsByEmailAndIsGoogle(user.getEmail(), true);
		Response response = new Response();
		if (exists) {
			logService.create("User with email " + user.getEmail() + " found on google login", LogLevel.INFO);
			// You can login
			response.setMessage(Message.USER_FOUND.getMessage());
		} else {
			logService.create("User with email " + user.getEmail() + " not found on google login", LogLevel.INFO);
			
			// Create the user
			userService.create(user.getEmail(), user.getPassword(), true);
			
			logService.create("User with email " + user.getEmail() + " created on google login", LogLevel.INFO);
			
			user = userService.getByEmail(user.getEmail());
			
			response.setMessage(Message.USER_CREATED.getMessage());
		}
		
		// Create the JWT
		String compactJws = Jwts.builder()
		  .setSubject(user.getEmail())
		  .signWith(SignatureAlgorithm.HS512, Constant.SECRET_KEY)
		  .compact();
		
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		headers.add("Authorization", "Bearer " + compactJws);
		return new ResponseEntity<Response>(response, headers , HttpStatus.OK);
	}
}