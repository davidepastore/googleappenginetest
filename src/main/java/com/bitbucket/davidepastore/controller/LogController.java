package com.bitbucket.davidepastore.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bitbucket.davidepastore.model.Log;
import com.bitbucket.davidepastore.service.ILogService;

@Controller
@RequestMapping("api")
public class LogController {
	
	@Autowired
	private ILogService logService;

	@GetMapping("logs")
	public ResponseEntity<List<Log>> getAll() {
		List<Log> logs = logService.getAll();
		return new ResponseEntity<List<Log>>(logs, HttpStatus.OK);
	}
}