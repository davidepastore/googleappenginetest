package com.bitbucket.davidepastore.constant;

public enum Message {

	USER_FOUND("User found"), USER_NOT_FOUND("User not found"), USER_CREATED("User created");
	
	private String message;
	
	Message(String message){
		this.setMessage(message);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
