package com.bitbucket.davidepastore.constant;

/**
 * Application generic constants.
 * @author Davide
 *
 */
public class Constant {
	
	/**
	 * JWT secret key.
	 */
	public static final String SECRET_KEY = "secret-key";

}
