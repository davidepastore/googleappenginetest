package com.bitbucket.davidepastore.constant;

public enum LogLevel {

	SUCCESS("success"), INFO("info"), WARNING("warning"), DANGER("danger");
	
	private String level;
	
	LogLevel(String level){
		this.setLevel(level);
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
}
