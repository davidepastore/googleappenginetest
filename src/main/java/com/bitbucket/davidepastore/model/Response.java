package com.bitbucket.davidepastore.model;

/**
 * Generic object for the response.
 * @author Davide
 *
 */
public class Response {
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
