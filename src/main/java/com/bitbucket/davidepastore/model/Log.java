package com.bitbucket.davidepastore.model;

/**
 * Log model.
 * 
 * @author Davide Pastore
 *
 */
public class Log {

	private int id;

	private String text;

	private String level;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

}
