package com.bitbucket.davidepastore.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bitbucket.davidepastore.constant.LogLevel;
import com.bitbucket.davidepastore.converter.LogConverter;
import com.bitbucket.davidepastore.dao.ILogDAO;
import com.bitbucket.davidepastore.entity.Log;

@Service
public class LogService implements ILogService {
	@Autowired
	private ILogDAO logDAO;

	@Override
	public void create(String text, LogLevel level) {
		Log user = new Log();
		user.setText(text);
		user.setLevel(level.getLevel());
		logDAO.create(user);
	}

	@Override
	public List<com.bitbucket.davidepastore.model.Log> getAll() {
		List<Log> logs = logDAO.getAll();
		return LogConverter.fromEntityToModel(logs);
	}
}
