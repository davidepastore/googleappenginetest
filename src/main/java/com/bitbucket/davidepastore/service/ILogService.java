package com.bitbucket.davidepastore.service;

import java.util.List;

import com.bitbucket.davidepastore.constant.LogLevel;
import com.bitbucket.davidepastore.model.Log;

public interface ILogService {
	void create(String text, LogLevel level);
	
	List<Log> getAll();
}
