package com.bitbucket.davidepastore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bitbucket.davidepastore.dao.IUserDAO;
import com.bitbucket.davidepastore.entity.User;

@Service
public class UserService implements IUserService {
	@Autowired
	private IUserDAO userDAO;
	
	@Override
	public User getByEmail(String email) {
		User obj = userDAO.getByEmail(email);
		return obj;
	}

	@Override
	public User getByEmailAndPassword(String email, String password) {
		User obj = userDAO.getByEmailAndPassword(email, password);
		return obj;
	}

	@Override
	public boolean exists(String email, String password) {
		return userDAO.exists(email, password);
	}

	@Override
	public boolean existsByEmailAndIsGoogle(String email, Boolean isGoogle) {
		return userDAO.exists(email, isGoogle);
	}

	@Override
	public void create(String email, String password, Boolean isGoogle) {
		User user = new User();
		user.setEmail(email);
		user.setPassword(password);
		user.setIsGoogle(isGoogle);
		userDAO.create(user);
	}
}
