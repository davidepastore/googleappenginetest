package com.bitbucket.davidepastore.service;

import com.bitbucket.davidepastore.entity.User;

public interface IUserService {
	User getByEmail(String email);

	User getByEmailAndPassword(String email, String password);

	boolean existsByEmailAndIsGoogle(String email, Boolean isGoogle);

	boolean exists(String email, String password);

	void create(String email, String password, Boolean isGoogle);
}
