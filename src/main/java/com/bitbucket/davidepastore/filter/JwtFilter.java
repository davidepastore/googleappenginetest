package com.bitbucket.davidepastore.filter;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.filter.GenericFilterBean;

import com.bitbucket.davidepastore.constant.Constant;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;

/**
 * JWT Filter.
 * @author Davide
 *
 */
public class JwtFilter extends GenericFilterBean {
	
	private static ArrayList<String> FREE_ACCESS_URLS = new ArrayList<String>() {{
		add("/api/login");
		add("/api/google-login");
	}};

	@Override
	public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
			throws IOException, ServletException {
		final HttpServletRequest request = (HttpServletRequest) req;
		
		// If it's not a free access page
		if(!hasFreeAccess(request.getRequestURI())) {
			
			// Check for Authorization header
			final String authHeader = request.getHeader("Authorization");
			if (authHeader == null || !authHeader.startsWith("Bearer ")) {
				throw new ServletException("Missing or invalid Authorization header.");
			}

			final String token = authHeader.substring(7); // The part after "Bearer "

			try {
				final Claims claims = Jwts.parser().setSigningKey(Constant.SECRET_KEY).parseClaimsJws(token).getBody();
				//request.setAttribute("claims", claims);
			} catch (final SignatureException e) {
				throw new ServletException("Invalid token.");
			}
		}

		chain.doFilter(req, res);
	}
	
	/**
	 * Returns true if the page has free access.
	 * @param uri The URI to check.
	 * @return Returns true if the page has free access.
	 */
	private boolean hasFreeAccess(String uri) {
		for (String freeAccessUrl : FREE_ACCESS_URLS) {
			if(uri.contains(freeAccessUrl)) {
				return true;
			}
		}
		
		return false;
	}
}
