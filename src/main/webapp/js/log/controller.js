angular
    .module("GoogleAppTest")
    .controller("LogController", ['$log', 'logService',
    	function($log, logService) {
    var self = this;

    /**
     * Load all the logs.
     */
    self.loadAll = function() {
        logService.loadAll().then(function(response){
        	$log.log('Success response', response);
        	self.logs = response.data;
        	
        }, function(response){
        	$log.log('Error response', response); 
        });
    };
    
    self.loadAll();
}]);