angular
	.module('GoogleAppTest')
	.factory('logService', [ '$http', function($http) {
	return {
	
		/**
		 * Load all the logs.
		 * @return Returns a promise with the response.
		 */
		loadAll: function(){
			return $http.get('/api/logs/'); 
		}
	};
} ]);