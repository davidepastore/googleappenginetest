angular
	.module('GoogleAppTest')
	.factory('userService', [ '$http', function($http) {
	return {
		
		/**
		 * Login the user by the given parameters.
		 * @param email The email.
		 * @param password The password.
		 * @return Returns a promise with the response
		 */
		login: function(email, password) {
			var data = {
				email: email,
				password: password
			};
			return $http.post('/api/login', data); 
		},
	
		/**
		 * Launch the Google login.
		 * @param basicProfile The basic profile from the Google request.
		 * @return Returns a promise with the response.
		 */
		googleLogin: function(basicProfile, id){
			var data = {
				email: basicProfile.getEmail()
			};
			return $http.post('/api/google-login', data); 
		}
	};
} ]);