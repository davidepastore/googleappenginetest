angular
.module("GoogleAppTest")
.controller("HomeController", ['$scope', '$state', '$log', 'localStorageService',
	function($scope, $state, $log, localStorageService) {
	
	/**
	 * Logout.
	 */
	$scope.logout = function(){
		// Remove the JWT token and redirect in the login page
		localStorageService.remove('jwt-token');
		$state.go('login');
	};
}]);