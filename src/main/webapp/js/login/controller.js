angular
    .module("GoogleAppTest")
    .controller("LoginController", ['$scope', '$http', '$log', '$state', 'userService',
    	function($scope, $http, $log, $state, userService) {
    var self = this;
    self.error = false;

    /**
     * Login submit.
     */
    $scope.loginSubmit = function() {
        $log.log('Login submit!');
        self.error = false;
        
        userService.login(self.email, self.password).then(function(response){
        	$log.log('Success response', response);
        	
        	//Redirect to the next page
            $state.go('log');
        }, function(response){
        	$log.log('Error response', response); 
        	self.error = true;
        });
    };
    
    $scope.$on('event:google-plus-signin-success', function (event, authResult) {
    	// Send login to server or save into cookie
    	$log.log('Success', event, authResult, authResult.getBasicProfile(), authResult.getId());
    	
    	userService.googleLogin(authResult.getBasicProfile(), authResult.getId()).then(function(response){
        	$log.log('Success response for Google login', response);
        	
        	//Redirect to the next page
            $state.go('log');
        }, function(response){
        	$log.log('Error response', response); 
        	self.error = true;
        });
    });
    
    $scope.$on('event:google-plus-signin-failure', function (event, authResult) {
    	// Auth failure or signout detected
    	$log.log('Failure', event, authResult);
    });
}]);