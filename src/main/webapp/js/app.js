let app = angular.module("GoogleAppTest", ['ui.router', 'LocalStorageModule', 'directive.g+signin']);
app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
	//Register the jwtInterceptor
	$httpProvider.interceptors.push('jwtInterceptor');
	
	// Define the states
    var loginState = {
      name: 'login',
      url: '/',
      controller: 'LoginController as login',
      templateUrl: './partials/login.html'
    };

    var homeState = {
      name: 'home',
      url: '/home',
      controller: 'HomeController as home',
      templateUrl: './partials/home.html'
    };
    
    var logState = {
      name: 'log',
      url: '/log',
      controller: 'LogController as log',
      templateUrl: './partials/log.html'
    };
  
    $stateProvider.state('login', loginState);
    $stateProvider.state('home', homeState);
    $stateProvider.state('log', logState);

    //Default state
    $urlRouterProvider.otherwise("/")
  }]);

app.run(['$trace', '$transitions', '$log', 'menuService', function($trace, $transitions, $log, menuService){
	//Enable angular-ui-router transitions
	$trace.enable('TRANSITION');
	
	//Handle transitions between states
	$transitions.onStart({ }, function(trans) {
		currentMenuItem = menuService.findMenuItem(trans.to().name, trans.params('to'));
		menuService.setCurrentMenuItem(currentMenuItem);
		$log.log('trans', trans.to(), trans.from(), currentMenuItem);
	    trans.promise.finally(function(){
	    	menuService.onTransitionSuccess(trans.from());
	    });
	});
}]);