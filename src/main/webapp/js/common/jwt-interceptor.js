angular
	.module('GoogleAppTest')
	.factory('jwtInterceptor', ['localStorageService', '$log', '$q', function(localStorageService, $log, $q) {
	return {
		request : function(config) {
			// Use JWT if exists
			let jwtToken = localStorageService.get('jwt-token');
			if(jwtToken){
				$log.log('Using the JWT token', jwtToken);
				config.headers.Authorization = 'Bearer ' + jwtToken;
			}
			return config;
		},

		requestError : function(config) {
			return config;
		},

		response : function(res) {
			// Save JWT if exists
			let authorizationHeader = res.headers('Authorization');
			if(authorizationHeader){
				let jwtToken = authorizationHeader.substring(7); //Ignore Bearer string
				$log.log('Saving the JWT token', jwtToken);
	        	localStorageService.set("jwt-token", jwtToken);
			}
			return res;
		},

		responseError : function(res) {
			return $q.reject(res);
		}
	}
}]);