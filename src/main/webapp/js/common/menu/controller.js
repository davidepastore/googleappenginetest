angular
	.module('GoogleAppTest')
	.controller('menuController', [ '$state', 'menuService', function($state, menuService) {
		var vm = this;
		vm.menuItems = menuService.menuItems;

		/**
		 * Get route sref from the given menuItem.
		 * @param menuItem The menu item.
		 * @return Returns the sref from the given menuItem.
		 */
		vm.getRouteSref = function(menuItem) {
			if (!menuItem) {
				menuItem = vm.menuItems[0];
			}
			return $state.href(menuItem.route);
		};
	} ]);