angular
	.module('GoogleAppTest')
	.factory('menuService', ['$log', '$transitions', function($log, $transitions) {
		var menuItems = [ {
			name : 'login',
			heading : "Login",
			route : "login",
			active : false
		}, {
			name : 'home',
			heading : "Home",
			route : "home",
			active : false
		}, {
			name : 'log',
			heading : "Log",
			route : "log",
			active : false
		} ];
		var currentMenuItem;
		
		/**
		 * Get the current menu item.
		 * @return Returns the current menu item.
		 */
		getCurrentMenuItem = function(){
			return currentMenuItem;
		};
		
		/**
		 * Set the current menu item.
		 * @param menuItem The new menu item.
		 */
		setCurrentMenuItem = function(menuItem){
			currentMenuItem = menuItem;
		};
		
		/**
		 * Reset the given menu item.
		 */
		resetMenuItem = function(menuItem) {
			menuItem.active = false;
		};
		
		/**
		 * Reset all the menu items.
		 */
		resetMenuItems = function() {
			for (var i = 0; i < menuItems.length; i++) {
				resetMenuItem(menuItems[i]);
			}
		};
		
		/**
		 * Find a menu item from the given route name.
		 * @param routeName The route name.
		 * @return Returns the found menu item.
		 */
		findMenuItem = function(routeName) {
			var criteriaFunction = function(c) {
				return c.route === routeName || routeName.indexOf(c.route) != -1;
			};
			return menuItems.filter(criteriaFunction)[0];
		};
		
		/**
		 * Set the internal state of the service (active or inactive menu).
		 * @param fromState The state the request come from.
		 */
		onTransitionSuccess = function(fromState) {
			if (currentMenuItem) {
				currentMenuItem.active = true;
				prevMenuItem = findMenuItem(fromState.name);
				if (prevMenuItem && prevMenuItem.name !== currentMenuItem.name) {
					prevMenuItem.active = false;
				}
			} else {
				for (var i = 0; i < menuItems.length; i++) {
					menuItems[i].active = false;
				}
			}
		};

		return {
			menuItems: menuItems,
			currentMenuItem: getCurrentMenuItem(),
			setCurrentMenuItem: setCurrentMenuItem,
			onTransitionSuccess: onTransitionSuccess,
			findMenuItem: findMenuItem
		};
	}]);