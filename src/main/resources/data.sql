INSERT INTO `users` (`id`, `email`, `password`, `isgoogle`) VALUES
	(1, 'admin@mail.com', SHA2('password', 512), 0);

INSERT INTO `logs` (`id`, `text`, `level`) VALUES
	(1, 'Il primo log!', 'info'),
	(2, 'Pericolosissimo', 'danger'),
	(3, 'Attenzione!', 'warning'),
	(4, 'Successo :D', 'success');