--- Create database and tables
CREATE DATABASE IF NOT EXISTS `googleappenginetest`;

USE `googleappenginetest`;

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` CHAR(128),
  `isgoogle` BOOL DEFAULT FALSE,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `text` varchar(1000) NOT NULL,
  `level` CHAR(20)
  PRIMARY KEY (`id`)
);